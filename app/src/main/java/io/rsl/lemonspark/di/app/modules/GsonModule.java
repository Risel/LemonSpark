package io.rsl.lemonspark.di.app.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import dagger.Module;
import dagger.Provides;
import io.rsl.lemonspark.di.app.AppScope;

@Module
public class GsonModule {

    @AppScope
    @Provides
    public Gson gson(GsonBuilder gsonBuilder) {
        return gsonBuilder.create();
    }

    @AppScope
    @Provides
    public GsonBuilder gsonBuilder() {
        GsonBuilder builder = new GsonBuilder();
        //builder.excludeFieldsWithoutExposeAnnotation();
        return new GsonBuilder();
    }
}