package io.rsl.lemonspark.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.Objects;
import java.util.regex.Pattern;

import io.rsl.lemonspark.R;

import static io.rsl.lemonspark.utils.StringUtils.valueOf;

public class SignUpDialog {

    private Context context;
    private AlertDialog dialog;
    private Pattern emailPattern;

    private OnConfirmListener listener;

    private TextInputLayout email;
    private TextInputLayout username;
    private TextInputLayout name;
    private TextInputLayout pass;
    private TextInputLayout passConf;

    public SignUpDialog(@NonNull Context context, OnConfirmListener listener) {
        this.context = context;
        this.listener = listener;
        this.emailPattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        init();
    }

    private void init() {
        View view = View.inflate(context, R.layout.dialog_signup, null);

        email = view.findViewById(R.id.email_inputlayout);
        username = view.findViewById(R.id.username_inputlayout);
        name = view.findViewById(R.id.name_inputlayout);
        pass = view.findViewById(R.id.pass_inputlayout);
        passConf = view.findViewById(R.id.passrepeat_inputlayout);

        dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setNegativeButton("", (dialog, which) -> dialog.dismiss())
                .create();

        Objects.requireNonNull(dialog.getWindow()).setBackgroundDrawableResource(R.color.colorPrimary);

        dialog.setOnShowListener(dialog -> {
            if(isCorrectData()) {
                dialog.dismiss();
                listener.onConfirm(valueOf(email), valueOf(username), valueOf(name), valueOf(pass));
            }
        });
    }

    public void show() {
        dialog.show();
    }

    private boolean isCorrectData() {
        boolean correct = true;

        if(Objects.requireNonNull(pass.getEditText()).length() < 4) {
            pass.setErrorEnabled(true);
            pass.setError("Слишком короткий пароль");
        } else pass.setErrorEnabled(false);

        if(!Objects.equals(valueOf(pass), valueOf(passConf))) {
            passConf.setErrorEnabled(true);
            passConf.setError("Пароли не совпадают");
            correct = false;
        } else passConf.setErrorEnabled(false);

        if (!emailPattern.matcher(valueOf(email)).matches()) {
            email.setErrorEnabled(true);
            email.setError("Некорректный email");
            correct = false;
        } else email.setErrorEnabled(false);

        if (Objects.requireNonNull(username.getEditText()).length() < 4) {
            username.setErrorEnabled(true);
            username.setError("Слишком короткое имя пользователя");
            correct = false;
        } else username.setErrorEnabled(false);

        if (Objects.requireNonNull(name.getEditText()).length() < 4) {
            name.setErrorEnabled(true);
            name.setError("Слишком короткое имя");
            correct = false;
        } else name.setErrorEnabled(false);

        return correct;
    }

    public interface OnConfirmListener {
        void onConfirm(String email, String username, String name, String pass);
    }
}
