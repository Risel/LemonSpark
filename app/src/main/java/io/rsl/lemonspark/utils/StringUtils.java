package io.rsl.lemonspark.utils;

import android.support.design.widget.TextInputLayout;

import java.util.Objects;

public class StringUtils {

    public static String valueOf(TextInputLayout textInputLayout) {
        return textInputLayout.getEditText().getText().toString();
    }

}
