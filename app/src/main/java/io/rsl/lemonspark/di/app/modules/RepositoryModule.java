package io.rsl.lemonspark.di.app.modules;

import dagger.Module;
import dagger.Provides;
import io.rsl.lemonspark.api.ApiService;
import io.rsl.lemonspark.db.dao.ContractDao;
import io.rsl.lemonspark.db.dao.CredentialDao;
import io.rsl.lemonspark.db.dao.TransactionDao;
import io.rsl.lemonspark.db.dao.UserDao;
import io.rsl.lemonspark.di.app.AppScope;
import io.rsl.lemonspark.repositories.ContractsRepository;
import io.rsl.lemonspark.repositories.CredentialsRepository;
import io.rsl.lemonspark.repositories.TransactionsRepository;
import io.rsl.lemonspark.repositories.UserRepository;

@Module(includes = {ApiServiceModule.class, DatabaseModule.class})
public class RepositoryModule {

    @AppScope
    @Provides
    public ContractsRepository contractsRepository(ContractDao contractDao, ApiService apiService, CredentialsRepository credentialsRepository) {
        return new ContractsRepository(contractDao, apiService, credentialsRepository);
    }

    @AppScope
    @Provides
    public CredentialsRepository credentialsRepository(CredentialDao credentialDao) {
        return new CredentialsRepository(credentialDao);
    }

    @AppScope
    @Provides
    public TransactionsRepository transactionsRepository(TransactionDao transactionDao) {
        return new TransactionsRepository(transactionDao);
    }

    @AppScope
    @Provides
    public UserRepository userRepository(UserDao userDao) {
        return new UserRepository(userDao);
    }
}