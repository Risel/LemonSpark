package io.rsl.lemonspark.screens.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import javax.inject.Inject;

import io.rsl.lemonspark.R;
import io.rsl.lemonspark.dialogs.SignUpDialog;

public class LoginActivity extends AppCompatActivity {

    @Inject
    ApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    void register(View view) {
        SignUpDialog dialog = new SignUpDialog(this, (email, username, name, pass) -> {

        });
        dialog.show();
    }

    void login(View view) {

    }
}
