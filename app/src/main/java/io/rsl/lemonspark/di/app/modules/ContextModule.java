package io.rsl.lemonspark.di.app.modules;

import android.content.Context;

import dagger.Module;
import dagger.Provides;
import io.rsl.lemonspark.di.app.AppScope;

@Module
public class ContextModule {

    private final Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @AppScope
    @Provides
    public Context context() {
        return context;
    }
}