package io.rsl.lemonspark.di.app.modules;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import io.rsl.lemonspark.api.ApiService;
import io.rsl.lemonspark.di.app.AppScope;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = {NetworkModule.class, GsonModule.class})
public class ApiServiceModule {

    private static final String API_URL = "http://165.227.135.82";

    @AppScope
    @Provides
    public ApiService apiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @AppScope
    @Provides
    public Retrofit retrofit(Gson gson, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl(API_URL)
                .build();
    }

}
